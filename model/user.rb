class User < ApplicationRecord

  self.table_name = '42users'
  self.primary_key = 'id'

  def recheck_stud
    make_stud
  end

  def recheck_roles
    make_roles
  end

  def make_all
    begin
      puts self.login
      make_stud
      make_roles
    rescue StandardError => e
      puts "Error with #{self.login} in make_all"
    end
  end

  private

  def make_stud
    hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    bot = FTBot.instance
    ft_server = bot.server(bot.server_id)
    begin
      retries ||= 0
      ft_user = bot.ft_client.get("/v2/users/#{self.login}").parsed
    rescue OAuth2::Error
      sleep(TOKEN_REQUEST_SECOND)
      retry if (retries += 1) < 5
      return
    end
    return if ft_server&.member(self.discordid)&.role?(hash_arg[:server]['student_role']) && !ft_user['cursus_users'].detect { |k, _| true if k["cursus_id"].to_i == 21 || k["cursus_id"].to_i == 1 }

    cursus = ft_user['cursus_users'].detect { |k, _| true if k["cursus_id"].to_i == 21 || k["cursus_id"].to_i == 1 }
    return unless cursus && cursus['begin_at'].to_time <= Time.now

    member = ft_server.member(self.discordid)
    member&.remove_role(hash_arg[:server]['cadet_role']) if member&.role?(hash_arg[:server]['cadet_role'])
    member&.remove_role(hash_arg[:server]['curious_role']) if member&.role?(hash_arg[:server]['curious_role'])
    member&.add_role(hash_arg[:server]['student_role'])
  end

  def make_roles
    hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    bot = FTBot.instance
    ft_server = bot.server(bot.server_id)
    begin
      retries ||= 0
      ft_user = bot.ft_client.get("/v2/users/#{self.login}").parsed
    rescue OAuth2::Error
      sleep(TOKEN_REQUEST_SECOND)
      retry if (retries += 1) < 5
      return
    end
    projects = []
    arr_roles = {}
    return unless ft_server&.member(self.discordid)&.role?(hash_arg[:server]['student_role'])

    member = ft_server.member(self.discordid)
    ft_user['projects_users'].each do |info|
      next if info["project"]["name"].capitalize_first.start_with?("Day", "Rush", "Hackathon", "Peer V", "Duration", "Contract Upload", "Entrepreneurship final", "Entrepreneurship mid", "Company", "Startup Internship - ", "Internship I - ", "Internship II - ", "Part_Time I - ", "Part_Time I ", "Startup Internship", "C Exam Alone In The Dark - ")

      projects << "#{info["project"]["name"]}⭐".capitalize_first if (info["cursus_ids"][0].to_i == 1 || info["cursus_ids"][0].to_i == 21) && info["status"].to_s == "finished" && info["validated?"] == true
    end

    ft_server.roles.each { |r| arr_roles[r.name] = r }

    projects.each do |p|
      arr_roles[p] = ft_server.create_role(name: p, colour: rand(16777215), mentionable: false, permissions: Discordrb::Permissions.new([])) if arr_roles[p].nil?
      member.add_role(arr_roles[p]) unless member.role?(arr_roles[p])
    end

  end
end

User.columns.each { |column|
  puts column.name
  puts column.type
}

puts "User Model Initialized !".green