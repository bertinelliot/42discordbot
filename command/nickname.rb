# SetNick : Simple command to change nickname. Display an error if the nickname can't be apply for any reason.

@bot.command(:setnick, min_args: 1) do |event, *nick|
  begin
    event.user.nick = nick.join(' ')
    event.message.react('✅')
  rescue StandardError => e
    event.respond e.message
  end
end

puts 'SetNickname CMD Loaded !'.green
