@bot.command(:eval, min_args: 1, required_permissions: [:ban_members], help_available: false) do |event, *args|
  next unless event.user.id == 472454954121035776

  eval(args.join(' '))
  event.message.react('✅')
end

@bot.command(:display_roles, required_permissions: [:ban_members], help_available: false) do |event|
  event.server.roles.each { |x| puts "Name: #{x.name} - ID: #{x.id} - Position: #{x.position}" }
  event.message.react('✅')
end

@bot.command(:react_role, required_permissions: [:ban_members], help_available: false) do |event|
  hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/react_role_config.yml")))
  event.message.delete
  @mongo_db[:react_role].drop
  collection = @mongo_db[:react_role]
  hash_arg.each do |k, v|
    message = "**__#{k.upcase}:__**\n\n"
    v.each { |name, data| message << ":#{name}::`#{data['name']}`\n\n" }
    sended = event.channel.send_message(message)
    collection.insert_one(message_id: sended.id, list: [])
    v.each do |_, data|
      sended.react(data['emoji'])
      collection.update_one({ message_id: sended.id},
                            '$set' => { list: collection.find(message_id: sended.id).first[:list] \
                              << { role_id: data['role_id'], emoji: data['emoji'], unset: data['toggle'] } })
    end
  end
  nil
end

puts 'Admin CMD Loaded !'.green