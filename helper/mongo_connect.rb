# frozen_string_literal: true

def mongo_connect
  return Mongo::Client.new(["#{ENV['FT_MONGO_ADRESSE']}:#{ENV['FT_MONGO_PORT']}"], database: ENV['FT_MONGO_DB']) if ENV['FT_DISCORD_PROD']

  hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
  Mongo::Logger.logger.level = Logger::FATAL if hash_arg[:mongo]['silence'] || ENV['FT_MONGO_SILENCE']
  m = Mongo::Client.new(["#{hash_arg[:mongo]['adresse']}:#{hash_arg[:mongo]['port']}"], database: hash_arg[:mongo]['db'])
  puts "Mongo Initialized !".green
  puts "Informations du serveur Mongo: #{m.inspect}".cyan
  m
rescue StandardError => e
  puts "Mongo error : #{e.backtrace} #{e}"
  puts "Mongo failed to Initialize !".red
end