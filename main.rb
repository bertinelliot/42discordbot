RBNACL_LIBSODIUM_GEM_LIB_PATH = ENV['RBNACL_LIBSODIUM_GEM_LIB_PATH'] if ENV['RBNACL_LIBSODIUM_GEM_LIB_PATH']

require 'date'
require 'securerandom'
require 'rubygems'
require 'bundler/setup'
require 'pry'
require 'oauth2'
require 'mysql2'
require 'net/http'
require 'uri'
require 'json'
require 'mongo'
require 'fileutils'
require 'discordrb'
require 'colorize'
require 'active_record'
require 'awesome_print'
require 'irb'

load 'class/ft_bot.rb'
load 'class/string.rb'

load 'helper/sql_connect.rb'
load 'helper/mongo_connect.rb'
load 'helper/ft_connect.rb'
load 'helper/caches.rb'
load 'helper/reaction.rb'

# Process start after the bot is fully prepared
def finish_init
  @bot.ready do
    # Launch when bot is ready
    track_nb_users
    init_users_roles_cache
    puts 'Bot fully initialized!'.blue
  end
end

# The main loop of the project, everything start from here and happen here.
def main
  puts 'Hello World !'.green

  @bot = FTBot.instance
  puts "Start connections...".blue
  @sql_db = sql_connect
  @mongo_db = mongo_connect
  @bot.ft_client = @ft_client = ft_connect

  puts "Start loading ActionRecord...".blue
  load './model/application.rb'
  puts 'Start loading models...'.blue
  Dir['./model/*'].each { |f| load f unless f == './model/application.rb' }

  puts "Start loading commands...".blue
  Dir['./command/*'].each { |f| load f }

  puts "Start loading recurents...".blue
  Dir['./recurrent/*'].each { |f| load f }

  finish_init

  @bot.run # bot.run(:async) is also an option
end

main