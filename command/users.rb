
@bot.command(:user, min_args: 1, max_args: 1, usage: 'user <xlogin>', description: 'Show a 42 user') do |event, login|
  begin
    user = @ft_client.get("/v2/users/#{login}").parsed
  rescue StandardError
    event.respond "Erreur, cet utilisateur n'existe pas."
    next
  end
  coa = @ft_client.get("/v2/users/#{login}/coalitions").parsed.first
  event.channel.send_embed do |embed|
    embed.title = user["displayname"].to_s
    embed.image = Discordrb::Webhooks::EmbedImage.new(url: user["image_url"])
    embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: 'https://www.defi-metiers.fr/sites/default/files/doc-kelios/Logo/2015/07/23/42_Final_sigle_seul_copie.png')
    embed.description = "Informations de #{user["login"]}"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: '42')
    embed.url = "http://profile.intra.42.fr/users/#{user["login"]}"
    embed.colour = (coa.nil? ? "#5c5c5c" : coa["color"].to_s)
    embed.add_field(name: 'Piscine', value: "#{user["pool_month"]} #{user["pool_year"]}") unless user["pool_month"].nil? && user["pool_year"].nil?
    embed.add_field(name: 'Staff', value: "I am a Staff member !") if user["staff?"] == true
    user["location"].nil? ? embed.add_field(name: 'Location', value: 'Unavailable') : embed.add_field(name: 'Location', value: user["location"].to_s)
    embed.add_field(name: 'Wallet', value: user["wallet"].to_s)
    user['cursus_users'].each do |cursus|
      embed.add_field(name: cursus['cursus']['name'].to_s, value: "#{cursus["level"]} / #{cursus["grade"]}")
    end
  end
end

puts 'Users CMD Loaded !'.green