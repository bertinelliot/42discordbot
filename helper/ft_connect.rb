TOKEN_REQUEST_SECOND = 1

def ft_connect
  return OAuth2::Client.new(ENV['FT_UID'], ENV['FT_SECRET'], site: "https://api.intra.42.fr") if ENV['FT_DISCORD_PROD']

  hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
  m = OAuth2::Client.new(hash_arg[:FT]['UID'], hash_arg[:FT]['SECRET'], site: "https://api.intra.42.fr")
  token = m.client_credentials.get_token
  puts "42 Oauth2 Initialized !".green
  puts "EndPoint du serveur Oauth2: #{m.site.inspect}".cyan
  token
rescue StandardError => e
  puts "Oauth2 error : #{e.backtrace} #{e}"
  puts "42 Oauth2 failed to Initialize !".red
end