class FTBot < Discordrb::Commands::CommandBot
  attr_accessor :server_id, :ft_client, :verify, :updater
  cattr_accessor :bot_instance

  def initialize
    hash_arg = loading_bot_object
    signal_handler
    super(token: hash_arg[:discord]['token'], client_id: hash_arg[:discord]['client_id'], prefix: hash_arg[:discord]['prefix'])
    self.server_id = hash_arg[:server]['ft']
    self.ft_client = nil
    self.verify = false
    self.updater = false
  end

  def self.instance
    @@bot_instance ||= new
  end

  private

  # Dangerous
  def new
    super
  end

  def loading_bot_object
    return { token: ENV['FT_DISCORD_TOKEN'], client_id: ENV['FT_DISCORD_CLIENT_ID'], prefix: ENV['FT_DISCORD_PREFIX'] } if ENV['FT_DISCORD_PROD']

    YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
  end

  # Catch signals
  def signal_handler
    Signal.trap(2) do
      puts "exiting..."
      @sql_db.close if @sql_db
      exit(1)
    end
  end
end