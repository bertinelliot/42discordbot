# Triggered when a reachable message is edited. Used to trigger commands on edit

@bot.message_edit do |event|
  args = event.message.content.split(' ')
  args.delete_at(0)
  @bot.commands.each_key do |key|
    @bot.commands[key].call(event, args) if event.message.content.scan(/^#{@bot.prefix}#{Regexp.quote(key)}/).count > 0
  end
  nil
end

puts 'EditInspection recurrent Loaded !'.green